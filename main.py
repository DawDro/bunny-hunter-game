# ---------------------------------------- OOP Version ---------------------------------
# Sign to get ready between rounds
# every round ufo jumps faster 

import pygame
import random
import sys
from tkinter import messagebox

class BunnyHunterGame:
    def __init__(self):

        # Window settings
        pygame.init()
        self.win = pygame.display.set_mode((900,700))
        pygame.display.set_caption("Bunny Hunter")

        # Background settings
        self.mars_bg = pygame.image.load("mars_bg.jpg")
        self.mars_bg_scaled = pygame.transform.scale(self.mars_bg, (900,700))

        # Ufo settings
        self.ufo = pygame.image.load("ufo.svg").convert_alpha()
        self.ufo_scaled = pygame.transform.scale(self.ufo, (60, 60))
        self.ufo_rect = self.ufo_scaled.get_rect()
        self.ufo_rect.topleft = (100,100)

        # Clock settings
        self.timer_event = pygame.USEREVENT
        self.clock = pygame.time.Clock()
        self.counter, self.text = 10, '10'.rjust(3)
        pygame.time.set_timer(pygame.USEREVENT, 1000)
        self.font = pygame.font.SysFont('Consolas', 30)

        # Ufo jumps
        self.cooldown = 2000
        self.last_jump = pygame.time.get_ticks()

        # Start settings
        self.score = 0
        self.lives = 3
        self.round = 1

        # Coursor settings
        pygame.mouse.set_visible(False)
        self.cursor_img = pygame.image.load("aim.png").convert_alpha()
        self.aim_scaled = pygame.transform.scale(self.cursor_img, (50, 50))
        self.cursor_img_rect = self.aim_scaled.get_rect()

        # Game start
        self.game_round()
    
    def lost_message(self):
        """Window to show while the player lost"""
        self.msg_box = messagebox.askyesno("You Lost!","Do you want to play again?")
        return self.msg_box
    
    def win_message(self):
        """Window to show while the player won"""
        self.msg_box = messagebox.askyesno("You Win!","Are you ready for the next round?")
        return self.msg_box
    
    def reset_game(self):
        """Reset game stats and timers when lost"""
        self.run = True
        self.score = 0
        self.lives = 3
        self.round = 1
        self.cooldown = 2000
        self.reset_timer()

    def spawn_ufo(self):
        """Spawning ufo picture for random position"""
        self.ufo_rect.topleft = (random.randint(0,840), random.randint(30,640))
        return self.ufo_rect.topleft
    
    def next_round(self):
        """Reset game stats and timers when won to prepare for new round"""
        self.score = 0
        self.round += 1
        self.cooldown -= 400
        self.reset_timer()

    def reset_timer(self):
        """Timers reset"""
        self.counter = 10
        self.text = str(self.counter).rjust(3)
        pygame.time.set_timer(self.timer_event, 0)
        pygame.event.clear(self.timer_event)
        pygame.time.set_timer(self.timer_event, 1000)

    def game_round(self):
        """The game itself with event handling and all information on the screen"""
        self.run = True
        while self.run:
            self.cursor_img_rect.center = pygame.mouse.get_pos()
            for self.event in pygame.event.get():
                now = pygame.time.get_ticks()
                if self.event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                if self.event.type == pygame.MOUSEBUTTONDOWN:
                    if self.ufo_rect.collidepoint(self.event.pos):
                        self.ufo_rect.topleft = self.spawn_ufo()
                        self.score += 1
                        self.last_jump = pygame.time.get_ticks()
                if self.event.type == self.timer_event:
                    if self.run == True: 
                        self.counter -= 1
                        self.text = str(self.counter).rjust(3)
                    if self.counter >= 0 and self.score >= 10 and self.lives > 0:
                        self.run = self.win_message()
                        if self.run == True:
                            self.next_round()
                        else:
                            pygame.quit()
                            sys.exit()
                    if (self.counter <= 0 or self.lives == 0) and self.score < 10:
                        self.run = self.lost_message()
                        if self.run == True:
                            self.reset_game()
                        else:
                            pygame.quit()
                            sys.exit()
                    if now - self.last_jump >= self.cooldown:
                        self.last_jump = now
                        self.ufo_rect.topleft = self.spawn_ufo()
                        self.lives -= 1

            
            
            self.win.fill((0,0,0))
            self.win.blit(self.font.render(f"Your score: {str(self.score)}", True, (211, 211, 211)), (300,0))
            self.win.blit(self.font.render(f"Your lives: {str(self.lives)}", True, (211, 211, 211)), (650,0))
            self.win.blit(self.font.render(f"Round: {str(self.round)}", True, (211, 211, 211)), (100,0))
            self.win.blit(self.font.render(self.text, True, (211, 211, 211)), (0, 0))
            self.win.blit(self.mars_bg_scaled,(0, 30))
            self.win.blit(self.ufo_scaled, self.ufo_rect.topleft)
            self.win.blit(self.aim_scaled, self.cursor_img_rect)
            pygame.display.flip()
            self.clock.tick(60)

if __name__ == "__main__":
    BunnyHunterGame()