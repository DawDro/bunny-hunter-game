# The Bunny Hunter Game - Ufo version on Mars

My version of Bunny Hunter Game with Ufo graphics. 

The game was created as exam for the next level in ZeroToJunior coaching program to practise and confirm programming skills in Python.

Main goal was to get familiar with pygame library and create game myself with the skills that I acquired during the study program.

**Game Video is presented below**

[Click](https://youtu.be/eIW0fARnw1E)